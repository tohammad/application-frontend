import React,  { Component } from 'react';
import ReactDOM from 'react-dom';
import JSONEditor from 'jsoneditor';
import CreateApplication from './application-create'
import AppApi from "./application-api";
import { Table, Button } from 'react-bootstrap';

class ApplicationList extends Component{
    constructor(){
        super();
        this.state = {
            createViewDisplay: false,
            detailViewDisplay: false,
            data:[{}],
            item:null
        };
        this.handleCreate = this.handleCreate.bind(this);
        this.handleItemDelete = this.handleItemDelete.bind(this);
    }
    loadAppConfigsFromServer() {
        const _self = this;
        _self.setState({ createViewDisplay: false });
        AppApi.getApplications().then(data => {
            debugger
            _self.setState({data: data })
        }).catch(err => {
            console.log(err);
        });
    }

    componentDidMount() {
        this.loadAppConfigsFromServer();
    }
     handleCreate=(status, data)=>{
        this.setState({
            createViewDisplay: status,
            item:data
        });
    }
     handleItemDelete=(item,index)=>{
        const _self = this;
        AppApi.deleteApplication(item._id).then(data => {
            this.loadAppConfigsFromServer();
        }).catch(err => {
            console.log(err);
        });
    }
    render() {
        return (
            <div>
                { (this.state.createViewDisplay) && <CreateApplication item={this.state.item} cb={this.handleCreate} fetch={this.loadAppConfigsFromServer}></CreateApplication>}
                { (!this.state.createViewDisplay) && <ConfigTable data={this.state.data} cb={this.handleCreate} handleDelete={this.handleItemDelete}/> }
            </div>
        );
    }
};

class ConfigTable extends Component{
    constructor(props){
        super(props);
        this.handleClose = this.handleClose.bind(this);
        this.renderItem = this.renderItem.bind(this);
        this.handleItemEdit = this.handleItemEdit.bind(this);
    }
    handleClose(){
        this.props.cb(true, null);
    }
    handleItemEdit(status, data){
        this.props.cb(true, data);
    }
    renderItem(itemRec, i) {
        return (<ConfigInfo key={i} index={i} item={itemRec} handleEditItem={this.handleItemEdit}  handleItemDelete={this.props.handleDelete} />);
    }
    render() {
        const items = this.props.data;
        var rows = items.map(this.renderItem);
        return (
            <div className="container">
                <Button bsStyle="info"  onClick={this.handleClose}>Add New</Button>
                <br/>
                <br/>
                <Table responsive>
                    <thead>
                    <tr>
                      
                        <th>Application Namespace</th>
                        <th>Application Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </Table>
            </div>);
    }
};

class ConfigInfo extends Component{
    constructor(props){
        super(props);
        this.state = { };
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    handleDelete() {
        const _self = this;
        _self.setState({display: false});
        this.props.handleItemDelete(this.props.item, this.props.index);
    }
    handleEdit() {
        this.props.handleEditItem(false, this.props.item);
    }
    render() {
        const _item = this.props.item;
         return (
                <tr className="text-left">
                    <td >{_item.name}</td>
                    <td>{_item.name}</td>
                    <td>
                        <Button type="submit" className="action" bsStyle="primary" onClick={this.handleEdit}>
                            Edit
                        </Button>

                        <Button type="submit" className="action" bsStyle="primary" onClick={this.handleDelete}>
                            Delete
                        </Button>
                    </td>
                </tr>
        );


    }
};

export default ApplicationList;