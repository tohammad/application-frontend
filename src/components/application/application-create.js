import React,  { Component } from 'react';
import ReactDOM from 'react-dom';
import JSONEditor from 'jsoneditor';
import axios from 'axios';
import AppApi from "./application-api";
import { Form, FormControl, FormGroup, Button, ControlLabel, Col } from 'react-bootstrap';
class CreateApplication extends Component{
    constructor(props){
        super(props);
        this.state = {
            namespace: '',
            name: '',
            editor : null,
            formErrors: {namespace: '', name: '', editor : null},
            namespaceValid: false,
            nameValid: false,
            formValid: false
        };
        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.getValidationState = this.getValidationState.bind(this);
    }
    componentDidMount() {
        var container = ReactDOM.findDOMNode(this.refs.jsonEditor);
        var options = {};
        var editor = new JSONEditor(container, options);
        debugger
        // set json
        const json = this.props.item;
        let item = {};
        if(json){
            debugger
            this.state.id = json._id;
            this.state.name = json.name;
            this.state.namespace = json.namespace;
            if(json['configuration']) {
                item = json['configuration'];
            }
        }
        else {
                item = {
                "Array": [1, 2, 3],
                "Boolean": true,
                "Null": null,
                "Number": 123,
                "Object": {"a": "b", "c": "d"},
                "String": "Hello World"
            };
        }
        editor.set(item);
        this.setState(function(){
            return {
                editor:editor
            }
        })
    }
    handleSubmit(event) {
        event.preventDefault();
        const _self = this;
        var editor = this.state.editor;
        var json = editor.get();
        var model = {
            namespace: this.state.namespace,
            name: this.state.name,
            configuration: json
        };
        debugger
        // update application
        if(this.state.id) {
            AppApi.updateApplication(this.state.id, model).then(data => {
                _self.setState(function(){
                    return {
                        namespace: data.namespace,
                        name: data.name,
                        configuration: data.configuration,
                        id: data.id
                    }
                })
                this.props.cb(false);
            }).catch(err => {
                console.log(err);
            });
        }
        // create application
        else {
            AppApi.createApplication(model).then(data => {
                _self.setState(function(){
                    return {
                        namespace: data.namespace,
                        name: data.name,
                        configuration: data.configuration,
                        id: data.id
                    }
                })
                this.props.cb(false);
            }).catch(err => {
                console.log(err);
            });
        }
    }

    
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({[name]: value});      
     }
    
    handleClose(){
        this.props.cb(false);
    }

    getValidationState(value) {
        if(value.length > 0)
            return 'success';
        else {
            return 'error';
        }
    }
    render() {
        return (
            
           <div className="App container">
                <Button type="submit" bsStyle="info" onClick={this.handleClose}>Back</Button>
                 <br/>
                <br/>
           <form>
                <FormGroup controlId="namespace" className="row" validationState={this.getValidationState(this.state.namespace)}>
                <Col componentClass={ControlLabel} sm={2} className="text-left">
                    Application Namespace
                </Col>
                <Col sm={10}>
                <FormControl
                            type="text"
                            value={this.state.namespace}
                            placeholder="Enter Application Namespace"
                            onChange={this.handleInputChange}
                            name="namespace" 
                            id="namespace" 
                />
                </Col>
                </FormGroup>

                <FormGroup controlId="name" className="row" validationState={this.getValidationState(this.state.name)}>
                <Col componentClass={ControlLabel} sm={2} className="text-left">
                    Application Name
                </Col>
                <Col sm={10}>
                <FormControl
                            type="text"
                            value={this.state.name}
                            placeholder="Enter Application Name"
                            onChange={this.handleInputChange}
                            name="name"
                            id="name" 
                />
                </Col>
                </FormGroup>

                <FormGroup controlId="jsonEditor" className="row">
                <Col componentClass={ControlLabel} sm={2} className="text-left">
                    Application Configuration
                </Col>
                 <Col sm={10}>
                    <div id="jsoneditor" ref="jsonEditor"></div>
                </Col>
                </FormGroup>
                 <Button type="submit" bsStyle="primary" onClick={this.handleSubmit} >
                    Save
                </Button>
        </form> 
    </div>
    );
    }
};

export default CreateApplication;