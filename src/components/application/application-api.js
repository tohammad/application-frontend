 import axios from "axios";
import { appConfig } from '../../config.js';
export default {

  getApplications: function(name) {
    debugger
    return axios.get(appConfig.baseUrl + '/applications')
      .then(response => {
        debugger
         return response.data;
      })
  },

  getApplicationDetails: function(id) {
    return axios.get(appConfig.baseUrl + `/application/${id}`)
      .then(response => {
        let data = response.data || {};
         return {
           namespace: data.namespace,
           name: data.name,
           configuration: data.configuration
         };
      })
  },

  createApplication: function(model) {
    return axios.post(appConfig.baseUrl + '/application/create', model)
      .then(response => {
        debugger
         return {
           namespace: response.data.namespace,
           name: response.data.name,
           configuration: response.data.configuration,
           id: response.data._id
         };
      })
  },

   updateApplication: function(id, model) {
    return axios.put(appConfig.baseUrl + `/application/${id}/update`, model)
      .then(response => {
        debugger
        let data = response.data || {};
         return {
           namespace: data.namespace,
           name: data.name,
           configuration: data.configuration
         };
      })
  },

   deleteApplication: function(id) {
     debugger
    return axios.delete(appConfig.baseUrl + `/application/${id}/delete`)
      .then(response => {
        debugger
         return response;
      })
  }

}