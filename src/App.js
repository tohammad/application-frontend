import React, { Component } from 'react';
import logo from './images/logo.svg';
import './css/App.css';
import ApplicationList from './components/application/application-list'
class App extends Component {
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          Application Configuration Management
        </p>
        <ApplicationList></ApplicationList>
      </div>
    );
  }
}

export default App;
